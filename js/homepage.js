// library for handling async requests
var request = window.superagent;

//all my templates go here
var home, projects, skills, vm;

var loadVue = function(html) {
  vm = new Vue({
    el: '#app',
    methods: {
      projects: function()
        {
          this.currentView = projects;
          // this.activeHome = '';
          this.activeSkills = '';
          this.activeProjects = 'active';
          $('.top-bar').removeClass('expanded');
        },
      skills: function()
      {
        this.currentView = skills;
        // this.activeHome = '';
        this.activeSkills = 'active';
        this.activeProjects = '';
        $('.top-bar').removeClass('expanded');
      },
      // home: function()
      // {
      //   this.currentView = home;
      //   this.activeHome = 'active';
      //   this.activeSkills = '';
      //   this.activeProjects = '';
      //   $('.top-bar').removeClass('expanded');
      // }
    },
    data: {
      currentView: html,
      // activeHome: 'active',
      activeProjects: 'active',
      activeSkills: ''
    }
  });

  Mousetrap.bind('p', function(){vm.projects();});
  Mousetrap.bind('h', function(){vm.home();});
  Mousetrap.bind('s', function(){vm.skills();});
};

// request.get('/templates/home.html', function(res){
//   home = res.text;
//   // console.log(home);
//   loadVue(home);
// });

request.get('/templates/projects.html', function(res){
  projects = res.text;
  loadVue(projects);
});

request.get('/templates/skills.html', function(res){
  skills = res.text;
});



// start the foundation functionality
$(document).foundation();

//get the html from certain web pages with this
/*request.get('/page/...', function (res) {
  // add the text from the response to the data
  //$('#content').html(res.text);
});*/
